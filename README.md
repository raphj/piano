A piano
=======

This piano features two movable keyboards and a "metronome".
You can also play using your AZERTY keyboard. Any patch to support other keyboards are welcome.

How to use
----------

It was possible, when I wrote this code, to just open index.html and use the piano. This is not
possible anymore with modern browsers for security reasons, so run a local HTTP server in this folder:

```sh
python -m http.server 7000
```

Open http://localhost:7000/index.html

and you are good to go. You can also host it on any HTTP server without any configuration.
Be careful with the CSP headers blocking features though. It must allow script-src: unsafe-inline (which is not ideal indeed).
Please apply this policy to this piano only, since it is a bit unsafe.

Suggested CSP:

```
Content-Security-Policy:
	frame-ancestors 'self';
	default-src 'none';
	connect-src 'self';
	script-src 'self' 'unsafe-inline';
	style-src 'self';
	base-uri 'none';
	form-action 'none';
	img-src 'self';
	media-src 'self' data:;
	object-src 'self'
```

Known issue
-----------

It is usable on a phone, though be careful not to leave it open, I noticed that it draws
the battery on Firefox Mobile a while ago. I haven't investigated much, maybe it's not an issue anymore, and haven't tried
if it is the same on Chrome-based browsers.
It probably prevents the sound channel to be released and thus, the phone to


Technical details
-----------------

This is really a quick and dirty code.
The piano is rendered using SVG and notes using MIDI.js.
