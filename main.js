/* @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later

    Copyright (c) Raphaël Jakse, 2016 - 2017
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

@license-end */

window.onload = function () {
    "use strict";

    /*var keys = {
        87:  [35, true],
        88:  [36, true],
        67:  [38, true],
        86:  [40, true],
        66:  [41, true],
        78:  [43, true],
        188: [45, true],
        59:  [47, true],
        58:  [48, true],
        161: [50, true],

        81:  [41, false],
        83:  [43, false],
        68:  [45, false],
        70:  [47, false],
        71:  [48, false],
        72:  [50, false],
        74:  [52, false],
        75:  [53, false],
        76:  [55, false],
        77:  [57, false],
        165: [59, false],
        170: [60, false],

        65:  [59, true],
        90:  [60, true],
        69:  [62, true],
        82:  [64, true],
        84:  [65, true],
        89:  [67, true],
        85:  [69, true],
        73:  [71, true],
        79:  [72, true],
        80:  [74, true],
        0:   [76, true],
        164: [77, true],

        49:  [65, false],
        50:  [67, false],
        51:  [69, false],
        52:  [71, false],
        53:  [72, false],
        54:  [74, false],
        55:  [76, false],
        56:  [77, false],
        57:  [79, false],
        48:  [81, false],
        169: [83, false],
        61:  [84, false]
    }*/

    var keys = {
        q:  [48, false],
        z:  [49, false],
        s:  [50, false],
        e:  [51, false],
        d:  [52, false],
        f:  [53, false],
        t:  [54, false],
        g:  [55, false],
        y:  [56, false],
        h:  [57, false],
        u:  [58, false],
        j:  [59, false],
        k:  [60, false],
        o:  [61, false],
        l:  [62, false],
        p:  [63, false],
        m:  [64, false],
        ù:  [65, false],
        $:  [66, false],
        "*":  [67, false]
    };

    var isMouseDown = 0;
    var disableNotes = false;
    var pressedNotes = new Set();
    var domNotesByTouchIdentifier = {};
    var domNoteFromNote = {}

    function keyCodeToNote(keyCode) {
        var key = keys[keyCode.toLowerCase()];

        if (!key) {
            return 0;
        }

        var shift = 0;

        if (shiftLocation & RIGHT) {
            shift += 12;
        }

        if (shiftLocation & LEFT) {
            shift -= 12;
        }

        return domNoteFromNote[key[0] + shift];
//         return domNoteFromNote[key[0] + (
//             (((shiftLocation & RIGHT) && !key[1])
//                 ||
//             ((shiftLocation & LEFT) && key[1]))
//                 ? 1
//                 : 0
//         )];
    }

    function noteOn(domNote) {
        if (!domNote) {
            return;
        }

        MIDI.noteOn(0, domNote._note, 0.75, 0);
        domNote.classList.add("active");
        pressedNotes.add(domNote._note);
    }

    function noteDown(domNote, withMousedown, e) {
        if (!domNote) {
            return;
        }

        isMouseDown = !disableNotes && (isMouseDown || withMousedown);
        if (isMouseDown) {
            if (e && e.changedTouches) {
                for (let touch of e.changedTouches) {
                    domNotesByTouchIdentifier[touch.identifier] = domNote;
                }

                e.preventDefault(); // necessary for mobile blink
            }

            noteOn(domNote);
        }
    }

    function noteOff(domNote) {
        if (!domNote) {
            return;
        }
        MIDI.noteOff(0, domNote._note, 0.75, 0);
        domNote.classList.remove("active");
        pressedNotes.delete(domNote._note);
    }

    var LEFT  = 1;
    var RIGHT = 2;
    var shiftLocation = 0;

    function getShiftLocation(e) {
        return (
            e.location === KeyboardEvent.DOM_KEY_LOCATION_LEFT
                ? LEFT
                : (
                    e.location === KeyboardEvent.DOM_KEY_LOCATION_RIGHT
                        ? RIGHT
                        : 0
                )
        )
    }

    function touchMove(e) {
        if (disableNotes) {
            return;
        }

        for (let touch of e.changedTouches) {
            let oldDomNote = domNotesByTouchIdentifier[touch.identifier];
            let newDomNote = document.elementFromPoint(touch.clientX, touch.clientY);
            let newDomNoteContainsNote = newDomNote.classList.contains("note");

            if (newDomNoteContainsNote && e.touches.length > 1) {
                e.preventDefault();
            }

            if (oldDomNote !== newDomNote) {
                if (oldDomNote) {
                    noteUp(oldDomNote);
                }

                if (newDomNoteContainsNote) {
                    noteDown(newDomNote, true);
                    domNotesByTouchIdentifier[touch.identifier] = newDomNote;
                } else {
                    domNotesByTouchIdentifier[touch.identifier] = null;
                }
            }
        }
    }

    var noteUp = function (domNote, e) {
        if (!domNote) {
            return;
        }
        noteOff(domNote);
        pressedNotes.delete(domNote._note);
    }

    MIDI.loadPlugin({
        soundfontUrl: "./soundfont/",
        instrument: "acoustic_grand_piano",
        onsuccess: function () {
            MIDI.setVolume(0, 10000);

            var pianoObject = document.getElementById("piano-svg");

            var pianoSvgRoot1 = pianoObject.contentDocument.rootElement;

            var pianoContainer1 = document.getElementById("piano1");
            pianoContainer1.appendChild(pianoSvgRoot1);

            pianoObject.parentNode.removeChild(pianoObject);

            pianoObject = null;

            var rects = [].slice.call(pianoSvgRoot1.getElementsByTagName("rect"));
            pianoSvgRoot1.textContent = "";

            var pianoSvgRoot2 = pianoSvgRoot1.cloneNode(true);

            var pianoContainer2 = document.getElementById("piano2");
            pianoContainer2.appendChild(pianoSvgRoot2);

            var origWidth = pianoSvgRoot1.width.baseVal.value;
            var offset = 0;

            var repeat = Math.ceil(document.body.offsetWidth * 0.9 / origWidth);

            var do48 = Math.ceil(repeat / 2);
            var firstNote = 72 - (12 * do48);
            var currentDo = firstNote;

            var notes = [0, 2, 4, 5, 7, 9, 11, 1, 3, 6, 8, 10];

            var touchSupported = Object.hasOwnProperty.call(window, "ontouchstart");

            if (touchSupported) {
                document.addEventListener("touchstart", function (e) {
                    isMouseDown++;

                    if (pressedNotes.size || e.touches.length !== 1 || !e.target.classList.contains("piano-container")) {
                        document.ontouchmove = null;
                    }

                    if (!e.changedTouches[0].target.classList.contains("note")) {
                        disableNotes = true;
                        for (let domNote of document.getElementsByClassName("active")) {
                            noteUp(domNote);
                        }
                    }
                });

                document.addEventListener("touchend", function (e) {
                    isMouseDown--;
                    for (let touch of e.changedTouches) {
                        if (domNotesByTouchIdentifier[touch.identifier]) {
                            noteUp(domNotesByTouchIdentifier[touch.identifier]);
                        }
                    }

                    if (!isMouseDown) {
                        document.ontouchmove = null;
                        disableNotes = false;
                    }
                });

                window.addEventListener("touchmove", touchMove);
            } else {
                document.addEventListener("mousedown", function () {
                    isMouseDown = true;
                });

                document.addEventListener("mouseup", function (e) {
                    isMouseDown = false;
                    document.onmousemove = null;
                });
            }

            document.addEventListener("keydown", function (e) {
                if (e.target && e.target.nodeName.toLowerCase() === "input") {
                    return;
                }

                if (e.keyCode === 16) {
                    shiftLocation |= getShiftLocation(e);
                } else {
                    noteOn(keyCodeToNote(e.key /*e.keyCode*/, e.ctrlKey || e.metaKey, e.shiftKey));
                }

                e.preventDefault();
            });

            document.addEventListener("keyup", function (e) {
                if (e.keyCode === 16) {
                    shiftLocation &= ~ getShiftLocation(e);
                } else {
                    noteOff(keyCodeToNote(e.key /*e.keyCode*/, e.ctrlKey || e.metaKey, e.shiftKey));
                }

                e.preventDefault();
            });

            for (var i = 0; i < repeat; i++) {
                for (var j = 0; j < rects.length; j++) {
                    var c1 = rects[j].cloneNode(true);
                    c1.x.baseVal.value += offset;

                    var c2 = c1.cloneNode(true);

                    var currentNote = currentDo + notes[j];

                    domNoteFromNote[currentNote] = c1;

                    for (let c of [c1, c2]) {
                        c._note = currentNote;

                        if (touchSupported) {
                            c.addEventListener("touchstart", noteDown.bind(null, c, true));
                        } else {
                            c.addEventListener("mouseenter", noteDown.bind(null, c, false));
                            c.addEventListener("mouseleave", noteUp.bind(null, c, false));
                            c.addEventListener("mousedown", noteDown.bind(null, c, true));
                            c.addEventListener("mouseup", noteUp.bind(null, c));
                        }
                    }

                    pianoSvgRoot1.appendChild(c1);
                    pianoSvgRoot2.appendChild(c2);
                }

                currentDo += notes.length;
                offset += origWidth;
            }

            pianoSvgRoot1.width.baseVal.value *= repeat;
            pianoSvgRoot2.width.baseVal.value *= repeat;

            pianoSvgRoot1.ondragstart = pianoSvgRoot2.ondragstart = function (e) {
                e.stopPropagation();
                return false;
            };

            var containerMove;
            if (touchSupported) {
                containerMove = function (container, origMouseX, origContainerX, origMouseY, origContainerY, e) {
                    container.style.top  = origContainerY + (e.touches[0].clientY - origMouseY) + "px";
                    container.style.left = origContainerX + (e.touches[0].clientX - origMouseX) + "px";
                };

                pianoContainer1.ontouchstart = pianoContainer2.ontouchstart = function(e) {
                    if (pressedNotes.size || e.touches.length !== 1 || !e.target.classList.contains("piano-container")) {
                        document.ontouchmove = null;
                        return;
                    }

                    document.ontouchmove = containerMove.bind(
                        null,
                        e.target,
                        e.touches[0].clientX,
                        parseInt(window.getComputedStyle(e.target).left, 10),
                        e.touches[0].clientY,
                        parseInt(window.getComputedStyle(e.target).top, 10)
                    );

                    e.preventDefault();
                };
            } else {
                pianoContainer1.onmousedown = pianoContainer2.onmousedown = function(e) {
                    if (pressedNotes.size || !e.target.classList.contains("piano-container")) {
                        document.onmousemove = null;
                        return;
                    }

                    document.onmousemove = containerMove.bind(
                        null,
                        e.currentTarget,
                        e.clientX,
                        parseInt(window.getComputedStyle(e.currentTarget).left, 10),
                        e.clientY,
                        parseInt(window.getComputedStyle(e.currentTarget).top, 10)
                    );
                };

                containerMove = function (container, origMouseX, origContainerX, origMouseY, origContainerY, e) {
                    container.style.top  = origContainerY + (e.clientY - origMouseY) + "px";
                    container.style.left = origContainerX + (e.clientX - origMouseX) + "px";
                };
            }
        }
    });

    var metroTO = 0;
    var metronomeDuration = 1000;
    var metronomeInput = document.getElementById("metronome-input");
    metronomeInput.value = metronomeDuration = parseInt(metronomeInput.value) || metronomeDuration;
    metronomeInput.onchange = function () {
        metronomeDuration = parseInt(metronomeInput.value) || metronomeDuration;
    };

    function stopMetronome() {
        var pulse = document.querySelector(".pulse");
        pulse.classList.remove("pulse-left");
        pulse.classList.remove("pulse-right");
        document.getElementById("metronome-toggle").value = "▶"
        clearInterval(metroTO);
        metroTO = 0;
    }

    function toggleMetronome() {
        if (metroTO) {
            stopMetronome();
        } else {
            startMetronome();
        }
    }

    function startMetronome() {
        stopMetronome();
        var pulse = document.querySelector(".pulse");
        pulse.classList.add("pulse-left");
        pulse.classList.remove("pulse-right");
        document.getElementById("metronome-toggle").value = "■"
        var localMetronomeDuration = metronomeDuration;
        metroTO = setInterval(
            function () {
                pulse.classList.toggle("pulse-left");
                pulse.classList.toggle("pulse-right");
                if (localMetronomeDuration !== metronomeDuration) {
                    stopMetronome();
                    toggleMetronome();
                }
            },
            metronomeDuration
        );
    }

    document.getElementById("metronome").onsubmit = function (e) {
        startMetronome();
        e.preventDefault();
        return false;
    };

    document.getElementById("metronome-toggle").onclick = toggleMetronome;
};